from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.logout_page, name='logout'),
    path('login/', views.login_page, name='login'),
    path('register/', views.register_page, name='register'),





]