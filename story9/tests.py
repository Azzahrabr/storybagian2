from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import Story9Config
from .views import login_page, register_page, logout_page
from .forms import LoginForm, RegisterForm
from .models import Login
from django.contrib.auth.models import User



# Create your tests here.

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story9Config.name, "story9")
        
class FormTest(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            "username": "Ajah",
            "password": "ponyo",
        })

        form_regist = RegisterForm(data={
            "username": "ajah",
            "email": "ajah@gmail.com",
            "password_first" : "ponyo",
            "password_again" : "ponyo",
        })
        self.assertTrue(form_regist.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_register = RegisterForm(data={})
        self.assertFalse(form_register.is_valid())

    def test_form_regist_is_exist(self):
        form_regist = RegisterForm(data={
            "username": "ajah",
            "email": "ajah@gmail.com",
            "password_first" : "ponyo",
            "password_again" : "ponyo",
        })
        form_regist = RegisterForm(data={
            "username": "ajah",
            "email": "ajah@gmail.com",
            "password_first" : "ponyo",
            "password_again" : "ponyo",
        })
        self.assertTrue(form_regist.is_valid())



class TestUrls(TestCase):
    def test_urls(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,200)
        

class TestHTML(TestCase):
    def test_html(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, "main/home.html")

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_page = reverse("story9:login")
        self.register_page = reverse("story9:register")
        self.logout_page = reverse("story9:logout")
        self.user_new = User.objects.create_user("ajah","ajah@gmail.com", password='ponyo')
        self.user_new.save()
        self.login = Login.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_page, {
            'username': 'ajah', 
            'password':'ponyo'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_GET_register(self):
        response = self.client.get(self.register_page, {
            'username': 'ajah', 
            'email': 'ajah@gmail.com', 
            'password_first':'ponyo', 
            'password_again':'ponyo'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    def test_register_login_post(self):
        #register
        response = self.client.post(self.register_page, data = {
            "username" : "ajah",
            "email" : "ajah@gmail.com",
            "password_first" : "ponyo",
            "password_again" : "ponyo",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "ajah",
            "email" : "ajah@gmail.com",
            "password_first" : "ponyo",
            "password_again" : "pon",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "ajahlagi",
            "email" : "ajahponyo@gmail.com",
            "password_first" : "ponyo",
            "password_again" : "ponyo",
        })

        response = self.client.post(self.login_page,data ={
            "username" : "ajah",
            "password" : "ponyo"
        })
        self.assertEquals(response.status_code,302)

    def test_not_register_yet(self):
        response = self.client.post(self.login_page,data ={
            "username" : "ajahjah",
            "password" : "ponyo"
        })
        self.assertEquals(response.status_code,200)

class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("ajah", password="ponyo")
        self.new_user.save()
        self.login = Login.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "ajah",
            password = "ponyo"
        )

    def test_instance_created(self):
        self.assertEqual(Login.objects.count(),1)

    def test_instance_is_correct(self):
        self.assertEqual(Login.objects.first().user,self.new_user)

    def test_to_string(self):
        self.assertIn("ajah",str(self.new_user.login))

