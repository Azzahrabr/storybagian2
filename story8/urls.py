from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.homepage8, name='homepage8'),
    path('data', views.fungsi_data, name='data'),


]
