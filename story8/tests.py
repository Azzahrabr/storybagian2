from django.test import TestCase, Client
from django.urls import resolve
from .apps import Story8Config
from .views import homepage8

# Create your tests here.

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story8Config.name, "story8")

class TestUrls(TestCase):
    def test_urls(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)
        

class TestHTML(TestCase):
    def test_html(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, "homepage8.html")

class TestViews(TestCase):
    def test_views(self):
        found = resolve('/story8/')
        self.assertEqual(found.func,homepage8)

    def test_fungsi_data(self):
        response = self.client.get('/story8/data?q=elsa')
        self.assertEqual(response.status_code,200)