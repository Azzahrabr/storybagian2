$(document).ready(() => {
    $("#keyword").val('')
        $.ajax({
            // url: "https://www.googleapis.com/books/v1/volumes?q=" + ketik,
            method: 'GET',
            url: 'data?q=theojames', 
            success: function(data){
                let listBuku = $('tbody');
                var array_items = data.items;
            
                for(i=0;i < array_items.length;i++){
                    var penulis = array_items[i].volumeInfo.authors;
                    var judul = array_items[i].volumeInfo.title;
                    var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;

                    if(gambar == false){
                        var img = $('<td>').text("-");
                    }
                    else{
                        var img = $('<td>').append("<img src=" + gambar + ">");
                    }

                    var ttl = $('<td>').append(judul);

                    if(penulis == false){
                        var aut = $('<td>').text("-");
                    }
                    else{
                        var aut = $('<td>').append(penulis);
                    }

                    var content_row = $('<tr>').append(img,ttl,aut);

                    $('tbody').append(content_row);


                }
                

            }

        });
    
    
    
    $("#keyword").keyup(function(){
        var ketik = $("#keyword").val();
        console.log(ketik);
        let listBuku = $('tbody');
        //Teknik Ajax
        if(ketik.length){
            $.ajax({
                // url: "https://www.googleapis.com/books/v1/volumes?q=" + ketik,
                url: 'data?q=' + ketik,
                success: function(data){
                    // let listBuku = $('tbody');
                    var array_items = data.items;
                    $('tbody').empty();
                    // listBuku.empty();
                    // console.log(array_items);
                    for(i=0;i < array_items.length;i++){
                        var penulis = array_items[i].volumeInfo.authors;
                        var judul = array_items[i].volumeInfo.title;
                        var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;

                        if(gambar == false){
                            var img = $('<td>').text("-");
                        }
                        else{
                            var img = $('<td>').append("<img src=" + gambar + ">");
                        }

                        var ttl = $('<td>').append(judul);

                        if(penulis == false){
                            var aut = $('<td>').text("-");
                        }
                        else{
                            var aut = $('<td>').append(penulis);
                        }

                        var content_row = $('<tr>').append(img,ttl,aut);

                        $('tbody').append(content_row);


                    }
                   

                }
    
            });
        };

        $(listBuku).empty();
    });

});