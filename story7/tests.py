from django.test import TestCase, Client
from django.urls import resolve
from .apps import Story7Config
from .views import homepage

# Create your tests here.

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story7Config.name, "story7")

class TestUrls(TestCase):
    def test_urls(self):
        found = resolve('/story7/')
        self.assertEqual(found.func,homepage)

class TestHTML(TestCase):
    def test_html(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, "homepage.html")

class TestViews(TestCase):
    def test_views(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code,200)