$( function() {
    $( "#accordionProfile" ).accordion({
        active : false,
        collapsible: true,
        heightStyle: "panel",
    });
    $( "#accordionActivities" ).accordion({
        active : false,
        collapsible: true,
        heightStyle: "panel",
    });
    $( "#accordionExperiences" ).accordion({
        active : false,
        collapsible: true,
        heightStyle: "panel",
    });
    $( "#accordionAchievements" ).accordion({
        active : false,
        collapsible: true,
        heightStyle: "panel",
    });

    $('.button-up').click(function(){
        var thisAccordion = $(this).parent().parent().parent();
        thisAccordion.insertBefore(thisAccordion.prev());
        event.stopPropagation(); 
        event.preventDefault(); 
    });

    $('.button-down').click(function(){
        var thisAccordion = $(this).parent().parent().parent();
        thisAccordion.insertAfter(thisAccordion.next());
        event.stopPropagation(); 
        event.preventDefault(); 
    });

} );